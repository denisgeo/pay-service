package ge.bog.payservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;

import ge.bog.payservice.entities.Account;
import ge.bog.payservice.entities.Payment;
import ge.bog.payservice.enums.PaymentStatus;
import ge.bog.payservice.services.PaymentService;

@RestController
public class PaymentController {
	
	@Autowired
	private PaymentService paymentService;

    @RequestMapping("/getPayments")
    @ResponseBody
    public List<Payment> getPayments(@RequestParam(name="paymentStatus", required=false, defaultValue="")String paymentStatusStr) {
    	
    	PaymentStatus paymentStatus = null;
    	paymentStatusStr = paymentStatusStr.trim();
    	if(!Strings.isNullOrEmpty(paymentStatusStr)) {
    		paymentStatus = PaymentStatus.valueOf(paymentStatusStr);
    	}
    	List<Payment> list = paymentService.getPaymentsWithStatus(paymentStatus, 200);
        return list;
    }
    
    @RequestMapping("/getAccounts")
    @ResponseBody
    public List<Account> getAccounts() {
    	List<Account> list = paymentService.getAccounts(200);
        return list;
    }

}
