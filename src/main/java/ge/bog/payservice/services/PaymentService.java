package ge.bog.payservice.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ge.bog.payservice.entities.Account;
import ge.bog.payservice.entities.AgentAccounts;
import ge.bog.payservice.entities.Payment;
import ge.bog.payservice.enums.PaymentStatus;
import ge.bog.payservice.repositories.PaymentDao;

@Service
@Transactional(value=TxType.REQUIRED)
public class PaymentService {
	
	@PersistenceContext 
	private EntityManager em;

    @Autowired
    private PaymentDao paymentDao;
    

    @Transactional(value=TxType.REQUIRED)
    public void createPayment(ge.bog.payservice.ws.Payment p) {
    	
    	Payment payment = new Payment();
    	    	
    	payment.setService(paymentDao.getService(p.getServiceId()));
    	payment.setTerminal(paymentDao.getTerminal(p.getTerminalId()));
    	payment.setAmount(p.getAmount());

    	paymentDao.createPayment(payment);
    	
    }

    
    @Transactional(value=TxType.REQUIRED)
    public void createAthorization(Long id) {
    	
    	Payment payment = paymentDao.getPayment(id);
    	
    	if(!PaymentStatus.CREATED.equals(payment.getPaymentStatus())) {
    		return;
    	}
    	
    	paymentDao.createAthorization(payment);
    	
    	em.lock(payment, LockModeType.WRITE);
    	AgentAccounts agentAccounts = paymentDao.getAgentAccounts(payment.getTerminal().getAgent().getId(), payment.getService().getId());
    	Account account = agentAccounts.getMainAccount();
    	em.lock(account, LockModeType.WRITE);
    	paymentDao.takeMoneyFromAccount(account, payment.getAmount());
    	
    	paymentDao.changePaymentStatus(payment, PaymentStatus.AUTHORIZED);
    	
    }
    
    
    /*გდახდის ასახვის ემულაცია*/
    @Transactional(value=TxType.REQUIRED)
    public void inBilling(Long id) {
    	
    	Payment payment = paymentDao.getPayment(id);
    	
    	if(!PaymentStatus.AUTHORIZED.equals(payment.getPaymentStatus())) {
    		return;
    	}
    	
    	long choise = Math.round(Math.random());
    	
    	em.lock(payment, LockModeType.WRITE);
    	if(choise == 1){
    		paymentDao.changePaymentStatus(payment, PaymentStatus.IN_BILING);
    	} else {
    		paymentDao.changePaymentStatus(payment, PaymentStatus.REJECTED);
    	}
    	
    }
    
    @Transactional(value=TxType.REQUIRED)
    public void createTransaction(Long id) {
    	
    	Payment payment = paymentDao.getPayment(id);
    	
    	if(!PaymentStatus.IN_BILING.equals(payment.getPaymentStatus())) {
    		return;
    	}
    	
    	paymentDao.createTrasaction(payment);
    	
    	em.lock(payment, LockModeType.WRITE);
    	Account account = payment.getService().getMainAccount();
    	em.lock(account, LockModeType.WRITE);
    	paymentDao.putMoneyToAccount(account, payment.getAmount());
    	paymentDao.changePaymentStatus(payment, PaymentStatus.HAS_TRANSACTION);	
    }
    
    @Transactional(value=TxType.REQUIRED)
    public void cancelAuthorization(Long id) {
    	
    	Payment payment = paymentDao.getPayment(id);
    	
    	if(!PaymentStatus.REJECTED.equals(payment.getPaymentStatus())) {
    		return;
    	}
    	
    	paymentDao.cancelAthorization(payment.getId());
    	em.lock(payment, LockModeType.WRITE);
    	
    	AgentAccounts agentAccounts = paymentDao.getAgentAccounts(payment.getTerminal().getAgent().getId(), payment.getService().getId());
    	Account account = agentAccounts.getMainAccount();
    	em.lock(account, LockModeType.WRITE);
    	
    	paymentDao.putMoneyToAccount(account, payment.getAmount());
    	
    	paymentDao.changePaymentStatus(payment, PaymentStatus.CANCELED);
    	
    	
    }
    
    @Transactional(value=TxType.NOT_SUPPORTED)
    public List<Payment> getPaymentsWithStatus(PaymentStatus paymentStatus, Integer limit) {
    	return paymentDao.getPaymentsWithStatus(paymentStatus, limit);
    }
    
    @Transactional(value=TxType.NOT_SUPPORTED)
    public List<Account> getAccounts(Integer limit) {
    	return paymentDao.getAccounts(limit);
    }

}
