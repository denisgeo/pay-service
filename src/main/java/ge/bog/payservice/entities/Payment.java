package ge.bog.payservice.entities;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import ge.bog.payservice.enums.PaymentStatus;

@Entity
@Table(name="PAYMENT")
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@ManyToOne
	@JoinColumn(name = "TERMINAL_ID", nullable=false)
	private Terminal terminal;

	@Column(name = "AMOUNT", nullable=false)
	private BigDecimal amount;

	@Column(name = "PAYMENT_STATS", nullable=false)
	private PaymentStatus paymentStatus;

	@ManyToOne
	@JoinColumn(name = "SERVICE_ID", nullable=false)
	private Service service;
	
	@OneToMany(mappedBy="payment")
	private Set<Authorization> authorizations;
	
	@OneToMany(mappedBy="payment")
	private Set<Transaction> transaction;

	public Set<Transaction> getTransaction() {
		return transaction;
	}

	public void setTransaction(Set<Transaction> transaction) {
		this.transaction = transaction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Set<Authorization> getAuthorizations() {
		return authorizations;
	}

	public void setAuthorizations(Set<Authorization> authorizations) {
		this.authorizations = authorizations;
	}

	@Override
	public String toString() {
		return String.format("Agent[id=%d, version=%d,  terminalNo='%s']", id, version, paymentStatus);
	}

}
