package ge.bog.payservice.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="AGENT_ACCOUNTS")
public class AgentAccounts {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Version
    private Integer version;
    
    @ManyToOne
    @JoinColumn(name="MAIN_ACCOUNT_ID", nullable=false)
    private Account mainAccount;
    
    @ManyToOne
    @JoinColumn(name="COMMISSION_ACCOUNT_ID", nullable=false)
    private Account commissionAccount;
    
    @ManyToOne
    @JoinColumn(name="SERVICE_ID", nullable=false)
    private Service service;
    
    @ManyToOne
    @JoinColumn(name="AGENT_ID")
    private Agent agent;


    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getVersion() {
		return version;
	}


	public void setVersion(Integer version) {
		this.version = version;
	}


	public Account getMainAccount() {
		return mainAccount;
	}


	public void setMainAccount(Account mainAccount) {
		this.mainAccount = mainAccount;
	}


	public Account getCommissionAccount() {
		return commissionAccount;
	}


	public void setCommissionAccount(Account commissionAccount) {
		this.commissionAccount = commissionAccount;
	}


	public Service getService() {
		return service;
	}


	public void setService(Service service) {
		this.service = service;
	}


	public Agent getAgent() {
		return agent;
	}


	public void setAgent(Agent agent) {
		this.agent = agent;
	}


	@Override
    public String toString() {
        return String.format(
                "Agent[id=%d, version=%d,  terminalNo='%s']",
                id, version, service);
    }

}
