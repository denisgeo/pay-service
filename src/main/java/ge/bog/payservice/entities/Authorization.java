package ge.bog.payservice.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="AUTHORIZATION")
public class Authorization {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Version
    private Integer version;
	
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name="PAYMETN_ID", nullable=false)
	private Payment payment;


    public Payment getPayment() {
		return payment;
	}


	public void setPayment(Payment payment) {
		this.payment = payment;
	}


	@Override
    public String toString() {
        return String.format(
                "Agent[id=%d, version=%d, terminalNo='%s', payment='%s']",
                id, version, payment);
    }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getVersion() {
		return version;
	}


	public void setVersion(Integer version) {
		this.version = version;
	}

}
