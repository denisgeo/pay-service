package ge.bog.payservice.schedulers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ge.bog.payservice.entities.Payment;
import ge.bog.payservice.enums.PaymentStatus;
import ge.bog.payservice.services.PaymentService;

@Component
public class PayScheduler {

    private static final Logger log = LoggerFactory.getLogger(PayScheduler.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    private PaymentService paymentService;

    @Async
    @Scheduled(fixedRate = 1000)
    public void createAuthorizationForPayment() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        
        List<Payment> payments = paymentService.getPaymentsWithStatus(PaymentStatus.CREATED, 200);
        
        payments.forEach(payment->{
        	paymentService.createAthorization(payment.getId());
        });
        
    }
    
    @Async
    @Scheduled(fixedRate = 1000)
    public void InBillingorFailForPayment() {
        log.info("The time is now {}", dateFormat.format(new Date()));

        List<Payment> payments = paymentService.getPaymentsWithStatus(PaymentStatus.AUTHORIZED, 200);
        
        payments.forEach(payment->{
        	paymentService.inBilling(payment.getId());
        });
    }
    
    @Async
    @Scheduled(fixedRate = 1000)
    public void createTransactionForPayment() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        
        List<Payment> payments = paymentService.getPaymentsWithStatus(PaymentStatus.IN_BILING, 200);
        
        payments.forEach(payment->{
        	paymentService.createTransaction(payment.getId());
        });
    }
    
    @Async
    @Scheduled(fixedRate = 1000)
    public void cancelAuthorizationForPayment() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        
        List<Payment> payments = paymentService.getPaymentsWithStatus(PaymentStatus.REJECTED, 200);
        
        payments.forEach(payment->{
        	paymentService.cancelAuthorization(payment.getId());
        });
    }

}
