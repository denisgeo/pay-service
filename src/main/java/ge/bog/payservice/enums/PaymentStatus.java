package ge.bog.payservice.enums;

public enum PaymentStatus {
	
	

		CREATED("შექმნილი"),
	    AUTHORIZED("ავტორიზაცია შექმნილი"),
	    IN_BILING("ბილინგში ასახული"),
	    REJECTED("უარყოფილი"),
	    HAS_TRANSACTION("ტრანზაქცია შექმნილი"),
	    CANCELED("გაუქმებული");
	
	private String name;
	
	private PaymentStatus(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
    @Override
    public String toString() {
        return String.format(
                "PaymentStatus[name='%s']", name);
    }

}
