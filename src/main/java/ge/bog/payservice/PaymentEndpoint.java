package ge.bog.payservice;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ge.bog.payservice.services.PaymentService;
import ge.bog.payservice.ws.CreatePaymentRequest;
import ge.bog.payservice.ws.CreatePaymentResponse;


@Endpoint
public class PaymentEndpoint {
	
	private static final String NAMESPACE_URI = "http://ws.payservice.bog.ge";
	
	@Autowired
	private PaymentService paymentService;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "createPaymentRequest")
	@ResponsePayload
	public CreatePaymentResponse createPayment(@RequestPayload CreatePaymentRequest p){
		paymentService.createPayment(p.getPayment());
		return new CreatePaymentResponse();
	}

}
