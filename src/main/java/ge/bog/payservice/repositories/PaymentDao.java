package ge.bog.payservice.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ge.bog.payservice.entities.Account;
import ge.bog.payservice.entities.AgentAccounts;
import ge.bog.payservice.entities.Authorization;
import ge.bog.payservice.entities.Payment;
import ge.bog.payservice.entities.Service;
import ge.bog.payservice.entities.Terminal;
import ge.bog.payservice.entities.Transaction;
import ge.bog.payservice.enums.PaymentStatus;

@Repository
public class PaymentDao {
	
	@PersistenceContext 
	private EntityManager em;
	
	public Service getService(Long id) {
		return em.find(Service.class, id);		
	}
	
	public Terminal getTerminal(Long id) {
		return em.find(Terminal.class, id);		
	}
	
	public void createPayment(Payment payment) {
		payment.setPaymentStatus(PaymentStatus.CREATED);
		em.persist(payment);		
	}
	
	
	public Payment getPayment(Long id) {
		return em.find(Payment.class, id);
	}
	
	public void takeMoneyFromAccount(Account account, BigDecimal amount) {
		account.setAmount(account.getAmount().subtract(amount));
		em.merge(account);
	}
	
	public void putMoneyToAccount(Account account, BigDecimal amount) {
		account.setAmount(account.getAmount().add(amount));
		em.merge(account);
	}
	
	
	public void changePaymentStatus(Payment payment, PaymentStatus paymentStatus) {
		payment.setPaymentStatus(paymentStatus);
		em.merge(payment);	
	}
	
	public void createAthorization(Payment payment) {
		Authorization authorization = new Authorization();
		authorization.setPayment(payment);
		em.persist(authorization);	
	}
	
	public void createTrasaction(Payment payment) {
		Transaction transaction = new Transaction();
		transaction.setPayment(payment);
		em.persist(transaction);	
	}
	
	public void cancelAthorization(Long paymentId) {
		Query q = em.createQuery("from Authorization c where c.payment.id = :paymentId");
		q.setParameter("paymentId", paymentId);
		List<Authorization> list = q.getResultList();
		if(!list.isEmpty()) {
			em.remove(list.get(0));
		}
		
	}
	
	public List<Payment> getPaymentsWithStatus(PaymentStatus paymentStatus, Integer limit) {
		
		String sql = "from Payment c";
		if(paymentStatus != null){
			sql+= " where c.paymentStatus = :paymentStatus";
		}
		Query q = em.createQuery(sql);
		if(paymentStatus != null) {
			q.setParameter("paymentStatus", paymentStatus);
		}
		q.setMaxResults(limit);
		List<Payment> list = q.getResultList();
		return list;
		
	}
	
	public List<Account> getAccounts(Integer limit) {
		
		Query q = em.createQuery("from Account");
		q.setMaxResults(limit);
		List<Account> list = q.getResultList();
		return list;
		
	}
	
	/*ამ მოდელში იგულისხმება რომ ერთ აგენტს ერთ სერვისზე შეიძლება ქონდეს ერთი წყვილი ანგარიში*/
	public AgentAccounts getAgentAccounts(Long agentId, Long serviceId) {
		
		Query q = em.createQuery("from AgentAccounts c where c.service.id = :serviceId and c.agent.id = :agentId");
		q.setParameter("agentId", agentId);
		q.setParameter("serviceId", serviceId);
		List<AgentAccounts> list = q.getResultList();
		if(list.isEmpty())  return null;
		return list.get(0);
		
	}

}
