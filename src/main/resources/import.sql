insert into account(id, version, amount) values(1, 1, 10000.00);
insert into account(id, version, amount) values(2, 1, 10000.00);
insert into account(id, version, amount) values(3, 1, 10000.00);
insert into account(id, version, amount) values(4, 1, 10000.00);
insert into account(id, version, amount) values(5, 1, 10000.00);
insert into account(id, version, amount) values(6, 1, 10000.00);

insert into agent(id, version, agent_name) values(1, 1, 'agent 1');
insert into agent(id, version, agent_name) values(2, 1, 'agent 2');
insert into agent(id, version, agent_name) values(3, 1, 'agent 3');


insert into terminal(id, version, terminal_no, agent_id) values(1, 1, 'terminal 1', 1);
insert into terminal(id, version, terminal_no, agent_id) values(2, 1, 'terminal 2', 2);
insert into terminal(id, version, terminal_no, agent_id) values(3, 1, 'terminal 3', 3);

insert into service(id, version, main_account_id) values(1, 1, 1);
insert into service(id, version, main_account_id) values(2, 1, 2);
insert into service(id, version, main_account_id) values(3, 1, 3);

insert into agent_accounts(id, version, main_account_id, commission_account_id, service_id, agent_id) values(1, 1, 4, 4, 1, 1);
insert into agent_accounts(id, version, main_account_id, commission_account_id, service_id, agent_id) values(2, 1, 5, 5, 2, 1);
insert into agent_accounts(id, version, main_account_id, commission_account_id, service_id, agent_id) values(3, 1, 6, 6, 3, 1);
