პროექტის გაშვება ხდება ge.bog.payservice.App class-ის main მეთოდის გამოძახებით

პროექტი იყენებს h2 in memory რელაციურ მონაცემთა ბაზას

application.properties ფაილში შემდეგი კონფიგურაციის მითითებით შეგიძლიათ გადართოთ jdbc connection-ი თქვენთვის სასურველ მონაცემთა ბაზაზე

spring.datasource.url=jdbc:mysql://localhost/test
spring.datasource.username=dbuser
spring.datasource.password=dbpass
spring.datasource.driver-class-name=com.mysql.jdbc.Driver


mysql-ის მაგალითი

spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect

spring.datasource.url = jdbc:mysql://localhost:3306/paymet_db?createDatabaseIfNotExist=true
spring.datasource.username = root
spring.datasource.password = root
spring.datasource.validationQuery = SELECT 1


webservice wsdl location: /ws/payment.wsdl (http://localhost:8080/ws/payment.wsdl)


soap request:

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.payservice.bog.ge">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:createPaymentRequest>
         <ws:payment>
            <ws:terminalId>1</ws:terminalId>
            <ws:amount>100.01</ws:amount>
            <ws:serviceId>1</ws:serviceId>
         </ws:payment>
      </ws:createPaymentRequest>
   </soapenv:Body>
</soapenv:Envelope>




გადახდის სტატუსის გადამოწმება
get all payments http://localhost:8080/getPayments

CREATED("შექმნილი"),
AUTHORIZED("ავტორიზაცია შექმნილი"),
IN_BILING("ბილინგში ასახული"),
HAS_TRANSACTION("ტრანზაქცია შექმნილი"),
CANCELED("გაუქმებული"),
REJECTED("უარყოფილი") ამ სტატუსის დამატება მომიწია სრული პროცესის სიმულაციისთვის.
	    
get payment by status http://localhost:8080/getPayments?paymentStatus=[CREATED|AUTHORIZED|IN_BILING|REJECTED|HAS_TRANSACTION|CANCELED]

ანგარიშების ნახვა შეიძლება 
http://localhost:8080/getAccounts

job-ბი ეშვება ასინქროლულად თითოებული მეთოდის გამოძახება ახალი thead-ია

